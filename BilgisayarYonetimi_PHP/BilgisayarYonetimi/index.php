<?php 
    session_start();
    ob_start();
    try{
        $db = new PDO("mysql:host=localhost; dbname=test; charset=utf8;", "root","");
    }
    catch(PDOException $e)
    {
        die($e->getMessage());
    }
    $selectState = $db->query("SELECT * FROM pc_state")->fetch(PDO::FETCH_OBJ);
?>




<!doctype html>
<html lang="tr">
  <head>
    <title>Bilgisayar Yönetimi</title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset"utf-8">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>


    <div class="container-fluid">
        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-body">
                    <h4 class="card-title">Bilgisayar Güvenliği</h4>
                    <p class="card-text">
                        <?php 
                            echo $selectState->state == 1 ? 'Aktif değil <br> <a href="?guvenlik=aktif_et"><b> Aktif Et </b></a>' : 'Aktif. <br> <a href="?guvenlik=devre_disi_birak"><b> Güvenliği Devre Dışı Bırak </b></a>' ;
                        
                            if(isset($_GET['guvenlik']))
                            {
                                switch($_GET['guvenlik'])
                                {
                                    case "aktif_et":
                                        $query = $db->prepare("update pc_state set state = ? ");
                                        $query->execute([0]);
                                        if($query)
                                        {
                                            header("location:/");
                                        }
                                    break;
                                    case "devre_disi_birak":
                                        $query = $db->prepare("update pc_state set state = ? ");
                                        $query->execute([1]);
                                        if($query)
                                        {
                                            header("location:/");
                                        }
                                    break;

                                    default:
                                        die("Geçersiz İşlem");
                                    break;
                                }
                            }

                        ?>
                    </p>
                </div>
                <br>
            </div>
            
            <div class="card col-md-12 col-xs-12">
                <div class="card-body">
                    <h4 class="card-title"> Bilgisayarım Kaç Saattir Açık ? </h4>
                    <p class="card-text"> <?=$selectState->active_date;?> </p> 
                </div>
            </div>

            <div class="card col-md-12 col-xs-12">
                <div clas="card-body">
                    <h4 class="card-title"> Bilgisayarda Takılı Olan Aygıtlar </h4>
                    <p class="card-text border p-3">
                        <?php 
                            $select_devices = $db->query("SELECT * FROM usb_devices")->fetchAll(PDO::FETCH_ASSOC);
                            foreach($select_devices as $row)
                            {
                            echo '<label> Cihaz ID: '.$row["device_id"].'</label> <br> ';
                            echo '<label> Cihaz : '.$row["device_description"].'</label> <br>';
                            }
                        ?>

                    </p>
                </div>
            </div>

            <div class="card col-md-12 col-xs-12">
                <div class="card-body">
                    <h4 class="card-title">Bilgisayara Mesaj Gönder</h4>
                    <p class="card-text">
                        <form action="" method="post">
                          <label>Mesaj Başlığı </label>
                          <input type="text" name="txtMesajBaslik" class="form-control" />
                          <label>Mesaj İçeriği </label>
                          <input type="text" name="txtMesajIcerigi" class="form-control" />
                          <button type="submit" name="btnMesajGonder" class="form-control btn btn-primary">Mesaj Gönder</button>
                        </form>
                        <?php 
                            if(isset($_POST['btnMesajGonder']))
                            {
                                $mesaj_baslik = $_POST['txtMesajBaslik'];
                                $mesaj_icerigi = $_POST['txtMesajIcerigi'];

                                if(empty($mesaj_baslik) or empty($mesaj_icerigi))
                                {
                                    echo '<div class="alert alert-warning"> Mesaj İçeriği veya Mesaj Başlığı Boş Olamaz!</div>';
                                }
                                else{
                                    $query = $db->prepare("INSERT INTO messages SET message_title = ?, message = ?");
                                    $query->execute([
                                        $mesaj_baslik,
                                        $mesaj_icerigi
                                    ]);

                                    if($query)
                                    {
                                        header("location:/");
                                    }
                                }
                            }
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>