﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilgisayarYonetimi.Forms
{
    public partial class MessageForms : Form
    {
        public MessageForms()
        {
            InitializeComponent();
        }
        Yonetim _yonetim = new Yonetim();
        private void MessageForms_Load(object sender, EventArgs e)
        {
            _yonetim.ListMessage(this.listView1);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                _yonetim.GetMessage(int.Parse(item.SubItems[0].Text.ToString()));
                richTextBox1.Text = Messages.Message;
            }

        }

        private void listView1_Click(object sender, EventArgs e)
        {

        }
    }
}
