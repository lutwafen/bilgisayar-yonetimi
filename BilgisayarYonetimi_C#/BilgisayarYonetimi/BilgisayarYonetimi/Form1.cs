﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilgisayarYonetimi
{
    public partial class Form1 : Form
    {
        Timer tmrControlDevice = new Timer();
        Timer tmrMessageControl = new Timer();
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }
        Yonetim _yonetim;
        Messages _mesaj;
        private void Form1_Load(object sender, EventArgs e)
        {

            //System.Threading.Thread.Sleep(120000);
            _mesaj = new Messages();
            _yonetim = new Yonetim();
            tmrUygulamaStart.Start();
            tmrUygulamaStart.Interval = 100;
            tmrGetUpTime.Start();
            tmrControlDevice.Interval = 600000;
            tmrControlDevice.Tick += TmrControlDevice_Tick;
            _yonetim.GetExtractDevice();
            lblMachineName.Text = _yonetim.GetMachineName();
            tmrMessageControl.Tick += TmrMessageControl_Tick;
            tmrMessageControl.Interval = 60000;
            tmrMessageControl.Start();
            tmrControlDevice.Start();
            string Mesaj = _yonetim.GetMessage();
            if (Mesaj != "0")
            {
                DialogResult result = MessageBox.Show(Mesaj + "\nMesaj Okundu Sayılsın mı?", Messages.MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    _yonetim.SetMessageViewed();
                }
            }
        }

        private void TmrProgramStart_Tick(object sender, EventArgs e)
        {

        }

        private void TmrMessageControl_Tick(object sender, EventArgs e)
        {
            string Mesaj = _yonetim.GetMessage();
            if (Mesaj != "0")
            {
                tmrMessageControl.Stop();
                DialogResult result = MessageBox.Show(Mesaj + "\nMesaj Okundu Sayılsın mı?", Messages.MessageTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    _yonetim.SetMessageViewed();
                }
                tmrMessageControl.Start();
            }
        }

        private void TmrControlDevice_Tick(object sender, EventArgs e)
        {
            _yonetim.GetExtractDevice();
        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void tmrUygulamaStart_Tick(object sender, EventArgs e)
        {
            _yonetim.StateGet();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            notifyIcon1.Visible = true;
            this.Hide();
        }

        private void tmrGetUpTime_Tick(object sender, EventArgs e)
        {
            _yonetim.updateGetUpTimeSystem();
        }

        private void tmrControlMessage_Tick(object sender, EventArgs e)
        {
            //_yonetim.GetMessage();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Forms.MessageForms mf = new Forms.MessageForms();
            mf.ShowDialog();
        }
    }
}
