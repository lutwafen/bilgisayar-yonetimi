﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using System.Data;
using System.IO;
using System.Management;
using System.Windows.Forms;

namespace BilgisayarYonetimi
{

    class Database
    {

        public MySqlConnection _con { get; set; }
        public MySqlCommand _Command { get; set; }
        public MySqlDataReader _Reader { get; set; }
        public Database()
        {
            try
            {
                _con = new MySqlConnection("Server=localhost; Database = test; Uid=root; Pwd=; SslMode=None");
            }
            catch (MySqlException mysqlEx)
            {
                System.Windows.Forms.MessageBox.Show(mysqlEx.ToString());
            }
        }

        public void DisposeCommand()
        {
            _Command.Dispose();
        }
        public void CloseReader()
        {
            _Reader.Close();
        }
        public void _ConOpen()
        {
            if (_con.State == ConnectionState.Closed)
            {
                _con.Open();
            }
            else
            {
                _con.Close();
                _con.Dispose();
                _con.Open();
            }
        }

        public void _ConClose()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
                _con.Dispose();
            }
            else
            {
                _con.Open();
                _con.Close();
                _con.Dispose();
            }
        }

        public void TRUNCATE_TABLE(string TabloAdi)
        {
            _ConOpen();
            _Command = new MySqlCommand();
            _Command.CommandText = "TRUNCATE TABLE " + TabloAdi;
            _Command.Connection = _con;
            _Command.ExecuteNonQuery();
            _Command.Dispose();
            _ConClose();
        }

    }

    class Messages
    {
        public static int MessageID { get; set; }
        public static string MessageTitle { get; set; }
        public static string Message { get; set; }
        public static int MessageView { get; set; }

        public static string MessageDate { get; set; }
    }

    class Yonetim : Database
    {

        void ProcStart()
        {
            Process proc = new Process();
            ProcessStartInfo _Info;
            _Info = new ProcessStartInfo("cmd.exe", "/C rundll32.exe user32.dll, LockWorkStation");
            _Info.CreateNoWindow = true;
            _Info.UseShellExecute = false;

            proc = Process.Start(_Info);
            proc.WaitForExit();
            proc.Close();
        }
        public void StateGet()
        {
            _ConOpen();

            MySqlCommand _StateGetCommand = new MySqlCommand("SELECT * FROM pc_state");
            _StateGetCommand.Connection = _con;
            MySqlDataReader _StateGetReader = _StateGetCommand.ExecuteReader();

            if (_StateGetReader.Read())
            {
                if (bool.Parse(_StateGetReader["state"].ToString()) == false)
                {
                    ProcStart();
                }
            }
            _StateGetCommand.Dispose();
            _StateGetReader.Close();
            _ConClose();
        }

        public string getUpTime()
        {
            String cevap = string.Empty;
            cevap += Convert.ToString(Environment.TickCount / 86400000) + " Gün ";
            cevap += Convert.ToString(Environment.TickCount / 3600000 % 24) + " Saat ";
            cevap += Convert.ToString(Environment.TickCount / 120000 % 60) + " Dakika ";
            cevap += Convert.ToString(Environment.TickCount / 1000 % 60) + " Saniye ";
            cevap += " Aktif.";

            return cevap;
        }

        public void updateGetUpTimeSystem()
        {
            String getUpTime = this.getUpTime();
            _con.Open();
            _Command = new MySqlCommand("UPDATE pc_state SET active_date = @active_date", _con);
            _Command.Parameters.AddWithValue("active_date", getUpTime.ToString());
            _Command.ExecuteNonQuery();
            _Command.Dispose();
            _con.Close();
            _con.Dispose();
        }


        public string GetMachineName()
        {
            //System.Windows.Forms.MessageBox.Show(Environment.MachineName);
            return Environment.MachineName;
        }

        public void GetExtractDevice()
        {
            RefreshExtractDevice();

            try
            {
                ManagementObjectSearcher device_searcher = new ManagementObjectSearcher("SELECT * FROM Win32_USBHub");
                foreach (ManagementObject usb_device in device_searcher.Get())
                {
                    _ConOpen();
                    _Command = new MySqlCommand("INSERT INTO usb_devices(device_id, device_description) VALUES (@device_id, @device_description)", _con);
                    //System.Windows.Forms.MessageBox.Show(usb_device.Properties["Description"].Value.ToString());
                    _Command.Parameters.AddWithValue("device_id", usb_device.Properties["PNPDeviceId"].Value.ToString());
                    _Command.Parameters.AddWithValue("device_description", usb_device.Properties["Description"].Value.ToString());
                    _Command.ExecuteNonQuery();
                    _Command.Dispose();
                    _ConClose();
                }
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message.ToString());
            }

        }

        public void ListMessage(ListView lstView)
        {
            _ConOpen();
            _Command = new MySqlCommand("SELECT * FROM messages", _con);
            _Reader = _Command.ExecuteReader();
            while (_Reader.Read())
            {
                ListViewItem item = new ListViewItem(_Reader["id"].ToString());
                item.SubItems.Add(_Reader["message_title"].ToString());
                item.SubItems.Add(_Reader["message_date"].ToString());
                if (_Reader["message_view"].ToString() == "1")
                {
                    item.SubItems.Add("Okundu");
                }
                else {
                    item.SubItems.Add("Okunmadı");
                }
                lstView.Items.Add(item);
            }
            _Reader.Close();
            _Command.Dispose();
            _ConClose();
        }

        public string GetMessage()
        {
            Messages _message = new Messages();
            _ConOpen();
            _Command = new MySqlCommand("SELECT * FROM messages WHERE message_view = 0", _con);
            _Reader = _Command.ExecuteReader();
            if (_Reader.Read())
            {

                Messages.MessageID = int.Parse(_Reader["id"].ToString());
                Messages.MessageTitle = _Reader["message_title"].ToString();
                Messages.Message = _Reader["message"].ToString();
                Messages.MessageView = int.Parse(_Reader["message_view"].ToString());
                Messages.MessageDate = _Reader["message_date"].ToString();
                return _Reader["message"].ToString();
            }
            _Reader.Close();
            _Command.Dispose();
            _ConClose();
            return "0";
        }
        public void GetMessage(int MessageID)
        {
            _ConOpen();
            _Command = new MySqlCommand("SELECT * FROM messages WHERE id = '" + MessageID + "' ", _con);
            _Reader = _Command.ExecuteReader();
            if (_Reader.Read())
            {
                Messages.Message = _Reader["message"].ToString();
            }
            _Reader.Close();
            _Command.Dispose();
            _ConClose();
        }

        public void SetMessageViewed()
        {
            System.Windows.Forms.MessageBox.Show(Messages.MessageID.ToString());
            _ConOpen();
            _Command = new MySqlCommand("UPDATE messages SET message_view = @message_view WHERE id = @id ", _con);
            _Command.Parameters.AddWithValue("message_view", 1);
            _Command.Parameters.AddWithValue("id", Messages.MessageID);
            _Command.ExecuteNonQuery();
            _Command.Dispose();
            _ConClose();
        }

        public void RefreshMessage()
        {  
            TRUNCATE_TABLE("messages");
        }

        public void RefreshExtractDevice()
        {
            TRUNCATE_TABLE("usb_devices");
        }
    }
}
