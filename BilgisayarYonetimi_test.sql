-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 18 Tem 2018, 21:39:09
-- Sunucu sürümü: 10.1.26-MariaDB
-- PHP Sürümü: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `test`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `message_title` varchar(256) CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `message_view` int(11) NOT NULL DEFAULT '0',
  `message_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `messages`
--

INSERT INTO `messages` (`id`, `message_title`, `message`, `message_view`, `message_date`) VALUES
(1, 'Test Mesajı', 'Bu mesaj test mesajıdır', 1, '2018-07-16 16:15:05'),
(2, 'Telefondan gönderilen mesaj ', 'Bu mesaj telefondan test için gönderilmiştir', 1, '2018-07-17 13:36:29');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pc_state`
--

CREATE TABLE `pc_state` (
  `state` tinyint(1) NOT NULL,
  `active_date` varchar(128) COLLATE utf8_turkish_ci NOT NULL,
  `Message` text COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `pc_state`
--

INSERT INTO `pc_state` (`state`, `active_date`, `Message`) VALUES
(1, '0 Gün 3 Saat 40 Dakika 21 Saniye  Aktif.', 'Şimdilik Bir Mesaj Yok! Bilginiz Olsun.');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `usb_devices`
--

CREATE TABLE `usb_devices` (
  `id` int(11) NOT NULL,
  `device_id` varchar(256) NOT NULL,
  `device_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `usb_devices`
--

INSERT INTO `usb_devices` (`id`, `device_id`, `device_description`) VALUES
(1, 'USB\\VID_0BDA&PID_0129\\20100201396000000', 'Realtek USB 2.0 Card Reader'),
(2, 'USB\\ROOT_HUB30\\4&1351F860&0&0', 'USB Kök Hub (USB 3.0)');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `usb_devices`
--
ALTER TABLE `usb_devices`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `usb_devices`
--
ALTER TABLE `usb_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
